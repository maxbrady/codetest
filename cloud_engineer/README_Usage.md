# Introduction

This set of scripts creates, lists, and deletes droplets.

# How to create droplets

Run script in bash

`bash droplet_create.sh`

It will prompt you for a name. 

Write what you would like to call your droplet.

# How to delete droplets

Run script in bash

`bash droplet_delete.sh`

It will prompt you for a name.

It will ask you if you're sure you want to delete the droplet.

# How to list droplets

Run script in bash

`droplet_list.sh`

It will list all droplets.
