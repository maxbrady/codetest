#!/bin/bash
#This script creates a Digital Ocean droplet.
#This is the droplet create section
NAME="$1"
if [ -n "$NAME" ]; then 
    echo "About to create server $NAME."
else 
    echo "ERROR: must specify server name on command line."
    exit 100
fi

doctl compute droplet create $NAME --image ubuntu-20-04-x64 --size s-1vcpu-1gb --region sfo3 --wait --ssh-keys 67:49:b6:d8:98:f4:38:15:89:82:18:46:ff:fc:ff:83
if [ $? -ne 0 ]
then
    echo "ERROR: droplet creation unsuccessful."
    exit 101   
fi

IP_ADDR=$(doctl compute droplet list | grep $NAME | awk '{ print $3}')
while :
do 
    ssh root@$IP_ADDR uptime > /dev/null 2>&1
    if [ $? -eq 0 ]; then 
        echo "Succesfully connected to server. Continuing..."
        break
    else 
        echo "Unable to connect to server. Trying again..."
    fi
    sleep 1
done 


ansible-playbook configure_todo.yml -i $IP_ADDR,

